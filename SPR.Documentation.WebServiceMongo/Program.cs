using HealthChecks.UI.Client;
using Hellang.Middleware.ProblemDetails;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using SPR.Documentation.WebServiceMongo.Settings;

const string DeepTag = "deep";

var logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

try
{
    logger.Debug("init main");

    var builder = WebApplication.CreateBuilder(args);

    // Add services to the container.
    builder.Services.AddProblemDetails();

    builder.Services.AddControllers();
    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();

    #region Health Checks
    var mongoSettings = GetMongoSettings(builder);
    builder.Services
        .AddHealthChecks()
        .AddMongoDb(MongoDB.Driver.MongoClientSettings.FromConnectionString(mongoSettings.ConnectionString), mongoSettings.DatabaseName, name: "mongo");
    #endregion

    var app = builder.Build();

    static MongoDbSettings GetMongoSettings(WebApplicationBuilder builder) => builder.Configuration.GetSection("MongoDb").Get<MongoDbSettings>();


    // Configure the HTTP request pipeline.
    #region Configure
    app.UseProblemDetails();

    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseHealthChecks("/health", new HealthCheckOptions
    {
        ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
    });
    app.UseHealthChecks("/health/lite", new HealthCheckOptions
    {
        ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse,
        Predicate = (p) => !p.Tags.Contains(DeepTag)
    });

    app.UseHttpsRedirection();

    app.UseAuthorization();

    app.MapControllers();
    #endregion

    app.Run();

}
catch (Exception exception)
{
    logger.Error(exception, "Stopped program because of exception");
    throw;
}
finally
{
    // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
    NLog.LogManager.Shutdown();
}

public class Startup { }