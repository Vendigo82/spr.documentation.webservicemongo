﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Documentation.WebServiceMongo.IntegrationTests
{
    public class WebFactoryBase : WebApplicationFactory<Startup>
    {
        //protected override IHostBuilder CreateHostBuilder()
        //{
        //    return base.CreateHostBuilder().UseEnvironment("Test");
        //}

        //protected override IHost CreateHost(IHostBuilder builder)
        //{
        //    builder.UseEnvironment("Test");
        //    return base.CreateHost(builder);
        //    //return base.use
        //}

        //protected override IHostBuilder CreateHostBuilder()
        //{
        //    return base.CreateHostBuilder()
        //        .ConfigureAppConfiguration(builder => builder.AddInMemoryCollection(new Dictionary<string, string>
        //        {
        //            ["TestKey"] = "123",
        //        }));
        //}

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            base.ConfigureWebHost(builder);

            //builder.ConfigureAppConfiguration()

            builder.ConfigureTestServices(services =>
            {
                services
                    .AddAuthentication("Test")
                    .AddScheme<AuthenticationSchemeOptions, TestAuthHandler>("Test", options => { });

                //services.AddAuthorization(options => options.ConfigurePolicies("Test"));
            });
        }
    }
}
